import { NgModule } from '@angular/core';
import { ModTwoComponent } from './mod-two.component';

@NgModule({
  declarations: [ModTwoComponent],
  imports: [
  ],
  exports: [ModTwoComponent]
})
export class ModTwoModule { }
